/*
* ���� ��� ��������
*
*
*
*
*
*
*
*/

#include "init_mcu.h"
#include "misc.h"
#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_pwr.h"
#include "stm32f10x_bkp.h"



GPIO_InitTypeDef GPIO_Init_Structure;
//GPIO_InitTypeDef GPIO_Init_Structure_PORTB;
USART_InitTypeDef UART_init_structure;

void init_gpio(void)
{
  
  //----Input Pull Down PORTA -----------------------------------------------
  GPIO_Init_Structure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_7 | GPIO_Pin_8;
  GPIO_Init_Structure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init_Structure.GPIO_Mode = GPIO_Mode_IPD;
  GPIO_Init(GPIOA,&GPIO_Init_Structure);
  
  //====Input Pull Up PORTA=====================================================
  GPIO_Init_Structure.GPIO_Pin = GPIO_Pin_4;
  GPIO_Init_Structure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init_Structure.GPIO_Mode = GPIO_Mode_IPU;
  GPIO_Init(GPIOA,&GPIO_Init_Structure);
  
    
  //====USART_RX_PIn_Config=====================================================
  GPIO_Init_Structure.GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_10;
  GPIO_Init_Structure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init_Structure.GPIO_Mode = GPIO_Mode_IPU;
  GPIO_Init(GPIOA,&GPIO_Init_Structure);

  //====USART_TX_PIn_Config=====================================================
  GPIO_Init_Structure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_9;
  GPIO_Init_Structure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init_Structure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(GPIOA,&GPIO_Init_Structure);
  
  
  //====ADC_PIn_Config==========================================================
  GPIO_Init_Structure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6;
  GPIO_Init_Structure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init_Structure.GPIO_Mode = GPIO_Mode_AIN;
  GPIO_Init(GPIOA,&GPIO_Init_Structure);
  
  //=============================PORTB==========================================
  
   //----Output PP pins  -----------------------------------------------
  GPIO_Init_Structure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_2 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_11 | GPIO_Pin_15;
   
  GPIO_Init_Structure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init_Structure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_Init(GPIOB,&GPIO_Init_Structure);
  
  //====Output OD pins==========================================================
  GPIO_Init_Structure.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_5;
  GPIO_Init_Structure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init_Structure.GPIO_Mode = GPIO_Mode_Out_OD;
  GPIO_Init(GPIOB,&GPIO_Init_Structure);
  
    
  //====Input PD=====================================================
  GPIO_Init_Structure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_6;
  GPIO_Init_Structure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init_Structure.GPIO_Mode = GPIO_Mode_IPD;
  GPIO_Init(GPIOB,&GPIO_Init_Structure);

  //====Input PU=====================================================
  GPIO_Init_Structure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14;
  GPIO_Init_Structure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init_Structure.GPIO_Mode = GPIO_Mode_IPU;
  GPIO_Init(GPIOB,&GPIO_Init_Structure);
}

void UART_config(void)
{
  
  //=======================USART_GSM============================================
  UART_init_structure.USART_BaudRate = 9600;
  UART_init_structure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  UART_init_structure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  UART_init_structure.USART_Parity = USART_Parity_No;
  UART_init_structure.USART_StopBits = USART_StopBits_1;
  UART_init_structure.USART_WordLength = USART_WordLength_8b;
  
  
  USART_Init(USART1,&UART_init_structure);
  //USART_Init(USART2,&UART_init_structure);
  
  USART_ITConfig(USART1,USART_IT_RXNE,ENABLE); 
  USART_Cmd(USART1, ENABLE);

  
    
};


void perepheral_clock(void)
{
  while (RCC_WaitForHSEStartUp()!=SUCCESS);
  RCC_HSEConfig(RCC_HSE_ON); //�������� ������� ���������
  RCC_SYSCLKConfig (RCC_SYSCLKSource_HSE); //������� ������� ��������� ���������� ���������� ������������
  RCC_HCLKConfig(RCC_SYSCLK_Div1); //��������� ������������ �������� 16���
  RCC_PCLK1Config (RCC_HCLK_Div1);      //����� ���� ����������� 16���
  RCC_PCLK2Config (RCC_HCLK_Div1);
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2 | RCC_APB1Periph_PWR | RCC_APB1Periph_BKP,ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB,ENABLE);
  RCC_HSICmd (DISABLE);
};


void rtc_config(void)
{
   PWR_BackupAccessCmd(ENABLE);
   BKP_DeInit();
   RCC_LSEConfig(RCC_LSE_ON);
/* Wait till LSE is ready */
   while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
     {
     };
   RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
   RCC_RTCCLKCmd(ENABLE);
   /* Wait for RTC registers synchronization */
   RTC_WaitForSynchro();
   /* Wait until last write operation on RTC registers has finished */
   RTC_WaitForLastTask();
 
   /* Enable the RTC Second */
   RTC_ITConfig(RTC_IT_SEC, ENABLE);
 
   /* Wait until last write operation on RTC registers has finished */
   RTC_WaitForLastTask();
 
   /* Set RTC prescaler: set RTC period to 1sec */
   RTC_SetPrescaler(33); /* RTC period = RTCCLK/RTC_PR = (32.768 KHz)/(32767+1) */
 
   /* Wait until last write operation on RTC registers has finished */
   RTC_WaitForLastTask();
  
   
};


void NVIC_config (void)
{
   NVIC_InitTypeDef NVIC_InitStructure;
 
   /* Configure one bit for preemption priority */
   NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
 
   /* Enable the RTC Interrupt */
   NVIC_InitStructure.NVIC_IRQChannel = RTC_IRQn;
   NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
   NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
   NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
   NVIC_Init(&NVIC_InitStructure);
//==================================USART======================================
  
   NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
   NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
   NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
   NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
   NVIC_Init(&NVIC_InitStructure);
  
  
  
  
  
  
  
};

